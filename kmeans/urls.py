from django.contrib import admin
from django.urls import path, include
from . import views


urlpatterns = [
    path('kmeans/', views.home, name='kmeans-home'),
    path('kmeans_geomentric_intuition/', views.geomentric_intuition, name='kmeans-gi'),
    path('random_initialization/', views.random_initialization, name='random-initialization'),
    path('centroid_assignment/', views.centroid_assignment, name='centroid-assignment'),
    path('re_compute/', views.re_compute, name='re-compute'),
    path('plot_data/', views.plot_data, name='plot-data'),
    path('pick_random_centroids/', views.pick_random_centroids, name='pick-random-centroids'),
    path('plot_clusters/', views.plot_clusters, name='plot-clusters'),
    path('plot_iterations/', views.plot_iterations, name='plot-iterations'),
]

