from django.core import paginator
from django.shortcuts import render
from django.core.paginator import Page, Paginator, PageNotAnInteger, EmptyPage
import urllib, base64
import io
import os
import json
import glob

import pandas as pd
import numpy as np
import random as rd
import matplotlib.pyplot as plt

# Create your views here.
def home(request):
    
    #Visualise data points
    data = pd.read_csv('./static/src/clustering.csv')

    X = data[["LoanAmount","ApplicantIncome"]]

    plt.scatter(X["ApplicantIncome"],X["LoanAmount"],c='black')
    plt.xlabel('AnnualIncome')
    plt.ylabel('Loan Amount (In Thousands)')

    fig = plt.gcf()
    # convert graph into dtring buffer and then we convert 64 bit code into image
    # buf = io.BytesIO()
    # fig.savefig(buf, format='png')
    # buf.seek(0)
    # string = base64.b64encode(buf.read())
    # uri_plot_data = urllib.parse.quote(string)

    files = glob.glob('./static/img/*')
    for f in files:
        os.remove(f)
    fig.savefig('./static/img/plot_data.png')
    # with open('/home/syamj/Desktop/kmeans/plt_json/uri_plot_data.json') as f:
    #     json.dumps({'data': uri_plot_data}, f)

    #number of clusters
    K=3

    # Select random observation as centroids
    Centroids = (X.sample(n=K))
    plt.close()
    plt.scatter(X["ApplicantIncome"],X["LoanAmount"],c='black')
    plt.scatter(Centroids["ApplicantIncome"],Centroids["LoanAmount"],c='red')
    plt.xlabel('AnnualIncome')
    plt.ylabel('Loan Amount (In Thousands)')

    fig = plt.gcf()

    fig.savefig('./static/img/plot_centroids.png')

    # with open('/home/syamj/Desktop/kmeans/plt_json/uri_plot_centroids.json') as f:
    #     json.dumps({'data': uri_plot_centroids}, f)


    # Step 3 - Assign all the points to the closest cluster centroid
    # Step 4 - Recompute centroids of newly formed clusters
    # Step 5 - Repeat step 3 and 4

    diff = 1
    j=0
    count = 0

    uri_plot_iter = {}

    while(diff!=0):
        count = count+1    
        XD=X
        i=1
        for index1,row_c in Centroids.iterrows():
            ED=[]
            for index2,row_d in XD.iterrows():
                d1=(row_c["ApplicantIncome"]-row_d["ApplicantIncome"])**2
                d2=(row_c["LoanAmount"]-row_d["LoanAmount"])**2
                d=np.sqrt(d1+d2)
                ED.append(d)
            X[i]=ED
            i=i+1
        
        # print(X.head(10))
        
        C=[]
        for index,row in X.iterrows():
            min_dist=row[1]
            pos=1
            for i in range(K):
                if row[i+1] < min_dist:
                    min_dist = row[i+1]
                    pos=i+1
            C.append(pos)
        X["Cluster"]=C
        # print(X.head(10))
        Centroids_new = X.groupby(["Cluster"]).mean()[["LoanAmount","ApplicantIncome"]]
        if j == 0:
            diff=1
            j=j+1
        else:
            diff = (Centroids_new['LoanAmount'] - Centroids['LoanAmount']).sum() + (Centroids_new['ApplicantIncome'] - Centroids['ApplicantIncome']).sum()
            print(diff.sum())
        Centroids = X.groupby(["Cluster"]).mean()[["LoanAmount","ApplicantIncome"]]

        color=['blue','green','cyan']
        plt.close()
        for k in range(K):
            data=X[X["Cluster"]==k+1]            
            plt.scatter(data["ApplicantIncome"],data["LoanAmount"],c=color[k])

        plt.scatter(Centroids["ApplicantIncome"],Centroids["LoanAmount"],c='red')
        plt.xlabel('Income')
        plt.ylabel('Loan Amount (In Thousands)')
        
        fig = plt.gcf()
        # convert graph into dtring buffer and then we convert 64 bit code into image
        # buf = io.BytesIO()
        fig.savefig('./static/img/plot_fig_'+str(count)+'.png')
        # buf.seek(0)
        # string = base64.b64encode(buf.read())
        # uri = urllib.parse.quote(string)

        plt.close()
        # with open('/home/syamj/Desktop/kmeans/plt_json/uri_plot_iter_'+str(count)+'.json') as f:
        #     json.dumps({'data': uri_plot_iter}, f)

    return render(request, 'kmeans/home.html', {'title':'K-Means'})

def geomentric_intuition(request):    
    return render(request, 'kmeans/geomentric_intuition.html', {'title':'K-Means | Geomentric Intuition'})

def random_initialization(request):    
    return render(request, 'kmeans/random_initialization.html', {'title':'K-Means | Random Initialization'})

def centroid_assignment(request):    
    return render(request, 'kmeans/centroid_assignment.html', {'title':'K-Means | Centroid Assignment'})

def re_compute(request):    
    return render(request, 'kmeans/re_compute.html', {'title':'K-Means | Centroid Update'})

def plot_data(request):    
    return render(request, 'kmeans/plot_data.html', {'title':'K-Means | Plotting Data'})

def pick_random_centroids(request):
    return render(request, 'kmeans/pick_random_centroids.html', {'title':'K-Means | Picking Random Centroids'})

def plot_iterations(request):

    plt_files = os.listdir('./static/img')
    no_of_iterations = len(plt_files)-4

    req_files = []
    for i in range(no_of_iterations):
        file_name = 'img/plot_fig_'+str(i+2)+'.png'
        req_files.append(file_name)

    paginator = Paginator(req_files, 2)
    page = request.GET.get('page')
    try:
        req_files = paginator.page(page)
    except PageNotAnInteger:
        req_files = paginator.page(1)
    except EmptyPage:
        req_files = paginator.page(paginator.num_pages)

    return render(request, 'kmeans/plot_iterations.html', {'req_files':req_files, 'page':page})

def plot_clusters(request):

    plt_files = os.listdir('./static/img')
    no_of_files = len(plt_files)-2

    req_file_name = 'img/plot_fig_'+str(no_of_files)+'.png'

    return render(request, 'kmeans/plot_clusters.html', {'data': req_file_name, 'title':'K-Means | Plotting Clusters'})

    

