from django.urls import path
from . import views

urlpatterns = [
	path('', views.home, name='home-index'),
	path('dashboard/', views.dashboard, name='dashboard'),
]